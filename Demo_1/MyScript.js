
let baseURL="https://api.themoviedb.org/3/";
let APIKey="4c044b363d1da6ae7914c97c14a29469";
function GetTopRated(){
  let url1="".concat(baseURL, 'movie/top_rated?api_key=',APIKey,'&language=en-US&page=1');
  fetch(url1)
  .then ((result1)=>{
      return result1.json();
  })
  .then ((data1)=>{
    $("#header1").empty();
    $("#header1").append(`
    <div class="header_box"> TOP RATED </div>
    `)
    $(".carousel-indicators").empty();
    $(".carousel-indicators").append(`
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    `)
    $(".carousel-inner").empty();
    const movie = data1.results[0];
    $(".carousel-inner").append(`
    <div class="carousel-item active">
      <img src="${"https://image.tmdb.org/t/p/original/"+movie.backdrop_path}" alt="${movie.title}" onclick="getDetail(${movie.id})">
      <div class="carousel-newcap">
        <div class="box1">${movie.title}</div>
        <div class="box2"><strong style="font-size:30px">Overview</strong></br>${movie.overview}</div>
      </div>   
    </div>`)
    for ( let i = 1; i<6;i++)
    {
      $(".carousel-indicators").append(`
    <li data-target="#demo" data-slide-to="${i}"></li>
    `)
    $(".carousel-inner").append(`
    <div class="carousel-item">
      <img src="${"https://image.tmdb.org/t/p/original/"+data1.results[i].backdrop_path}" alt="${data1.results[i].title}" onclick="getDetail(${data1.results[i].id})">
      <div class="carousel-newcap">
        <div class="box1">${data1.results[i].title}</div>
        <div class="box2"><strong style="font-size:30px">Overview</strong></br>${data1.results[i].overview}</div>
      </div>   
    </div>`)
    }
  })
}
function GetPopular(_pageNumber, par){  
  if(_pageNumber!=1)
  {
  document.getElementById("animated-gif").style.display="block"; 
  document.getElementById("full-content").style.display="none";
  }
  let url2="".concat(baseURL,'movie/popular?api_key=',APIKey,'&language=en-US&page=',_pageNumber);
  fetch(url2)
  .then ((result2)=>{
    return result2.json();
  })
  .then ((data2)=>{
    $("#header2").empty();
    $("#header2").append(`
    <div class="header_box"> POPULAR </div>
    `)
    $(".main_box").empty();

    for (const movie of data2.results)
    {
      let url2="".concat(baseURL,'movie/',movie.id,'?api_key=',APIKey,'&language=en-US');
    fetch(url2)
    .then ((result)=>{
      return result.json();
    })
    .then ((data)=>{
    $(".main_box").append(`
    <div class='card card_custom'>
     <img class='card-img-top' src='${"https://image.tmdb.org/t/p/original/"+movie.poster_path}' alt='${movie.title}' style='width:100%;height:450px'>
     <div class='card-body'>
        <p class='card-title'>${movie.title}</p>
        <p class='card-text'>
         <p><strong>Rated: </strong>${movie.vote_average}</p>
         <p><strong>Length: </strong>${data.runtime} minutes</p>
       </p>
      <a href='#' class='btn btn-success' onclick="getDetail(${movie.id})">More...</a>
     </div>
    </div>`);})
    }
    addPagination(data2.total_pages, _pageNumber, "GetPopular",par);
    if(_pageNumber!=1)
    setTimeout(function(){document.getElementById("animated-gif").style.display="none"; document.getElementById("full-content").style.display="block";var elmnt = document.getElementById("header2");elmnt.scrollIntoView();},2000);
  });
}
function _Search(_pageNumber,section){
  document.getElementById("animated-gif").style.display="block"; 
  document.getElementById("full-content").style.display="none";
    $(".main_box").empty();
    $("#_pagination").empty();
    $(".carousel").empty();
    $("#header2").empty();
    $("#header1").empty();
    document.getElementById("review").style.display="none";
    $("#header1").append(`
      <div class="header_box">Search Result </div>
    `);
  if (section==1)
    SearchMovie(_pageNumber);
  else 
  SearchPeople(_pageNumber);
  setTimeout(function(){document.getElementById("animated-gif").style.display="none"; document.getElementById("full-content").style.display="block";  document.getElementById("_select-Box").style.display="block";},2700);
}
function SearchMovie(_pageNumber){
  let x = document.getElementById("_search");
  if (x.value==="")
  {
    return;
  }
  let url="".concat(baseURL,'search/movie?api_key=',APIKey,'&language=en-US&page=',_pageNumber,'&include_adult=false&query=',x.value);
  fetch(url)
  .then ((result)=>{
    return result.json();
  })
  .then ((data)=>{
    if (data.errors)
    {
      return;
    }
    if (data.total_results=="0")
    {$(".main_box").append(`<div style="font-size:25px;font-weight:bold;color:white;">${x.value}: Movie doesn't exist. Try search by actor or return to <a href="Untitled-1.html">main page</a></div>`);
    return false;}
    for (const movie of data.results)
    {
      let url2="".concat(baseURL,'movie/',movie.id,'?api_key=',APIKey,'&language=en-US');
    fetch(url2)
    .then ((result)=>{
      return result.json();
    })
    .then ((data2)=>{
    $(".main_box").append(`
    <div class='card card_custom'>
     <img class='card-img-top' src='${"https://image.tmdb.org/t/p/original/"+movie.poster_path}' alt='${movie.title}' style='width:100%;height:450px'>
     <div class='card-body'>
        <p class='card-title'>${movie.title}</p>
        <p class='card-text'>
         <p><strong>Rated: </strong>${movie.vote_average}</p>
         <p><strong>Length: </strong>${data2.runtime} minutes</p>
       </p>
      <a href='#' class='btn btn-success' onclick="getDetail(${movie.id})">More...</a>
     </div>
    </div>`);})
    }
    addPagination(data.total_pages,_pageNumber,"_Search","1");
  });
  }

  function SearchPeople(_pageNumber){
    let x = document.getElementById("_search");
    if (x.value==="")
    {
      return;
    }
    let url="".concat(baseURL,'search/person?api_key=',APIKey,'&language=en-US&page=',_pageNumber,'&include_adult=false&query=',x.value);
    fetch(url)
  .then ((result)=>{
    return result.json();
  })
  .then ((data)=>{
    if (data.errors)
    {
      return;
    }
    if (data.total_results=="0")
    {$(".main_box").append(`<div style="font-size:25px;font-weight:bold;color:white;">${x.value}: Movie doesn't exist. Try search by name or return to <a href="Untitled-1.html">main page</a></div>`);
      return;}
    for (const movie of data.results)
    for(const _movie of movie.known_for)
    {
    let url2="".concat(baseURL,'movie/',_movie.id,'?api_key=',APIKey,'&language=en-US');
    fetch(url2)
    .then ((result)=>{
      return result.json();
    })
    .then ((data2)=>{
    $(".main_box").append(`
    <div class='card card_custom'>
     <img class='card-img-top' src='${"https://image.tmdb.org/t/p/original/"+_movie.poster_path}' alt='${_movie.title}' style='width:100%;height:450px'>
     <div class='card-body'>
        <p class='card-title'>${_movie.title}</p>
        <p class='card-text'>
         <p><strong>Rated: </strong>${_movie.vote_average}</p>
         <p><strong>Length: </strong>${data2.runtime} minutes</p>
       </p>
      <a href='#' class='btn btn-success' onclick="getDetail(${_movie.id})">More...</a>
     </div>
    </div>`);})
    }
    addPagination(data.total_pages,_pageNumber,"_Search","2");
  });
  }
  function addPagination(total_pages, currentPage, getFunction, par){
    $("#_pagination").empty();
    let _startnumb;
    let _endnumb;
    if(currentPage==1)
    $("#_pagination").append(`<li class="page-item disabled"><a class="page-link" href="#header2"">Previous</a></li>`);
    else $("#_pagination").append(`<li class="page-item"><a class="page-link" href="#header2" onclick="${getFunction}(${parseInt(currentPage)-1},${par})">Previous</a></li>`);
    if (currentPage<4)
    {
      _startnumb=1;
      _endnumb=7;
    }
    else if(currentPage>(total_pages-3))
    {
      _startnumb=total_pages-6;
      _endnumb=total_pages;
    }
    else
    {
      _startnumb=currentPage-3;
      _endnumb=currentPage+3;
    }
    for (let i = _startnumb; (i <= _endnumb) && (i <= total_pages);i++)
    if (i==currentPage)
    $("#_pagination").append(`
     <li class="page-item active"><a class="page-link" href="#header2">${i}</a></li>
    `);
    else
    $("#_pagination").append(`
     <li class="page-item"><a class="page-link" href="#header2" onclick="${getFunction}(${i},${par})">${i}</a></li>
    `);
    if (currentPage>=total_pages)
    $("#_pagination").append(`<li class="page-item disabled"><a class="page-link" href="#header2">Next</a></li>`);
    else $("#_pagination").append(`<li class="page-item"><a class="page-link" href="#header2" onclick="${getFunction}(${parseInt(currentPage)+1},${par})">Next</a></li>`);
  }
function getDetail(_movieid){
  let url="".concat(baseURL,'movie/',_movieid,'?api_key=',APIKey,'&language=en-US');
  fetch(url)
  .then ((result)=>{
    return result.json();
  })
  .then ((data)=>{
    $(".main_box").empty();
    $("#_pagination").empty();
    $(".carousel").empty();
    $("#header2").empty();
    $("#header1").empty();
    document.getElementById("review").style.display="none";
    let genre="";
    for (const i of data.genres)
    genre=genre.concat("<div class='_genresBox'>",i.name,"</div>","&nbsp");
    let url2="".concat(baseURL,'movie/',_movieid,'/credits?api_key=',APIKey);
    fetch(url2)
    .then ((result2)=>{
      return result2.json();
    })
    .then ((data2)=>{
      let actor="";
      let director="";
      for (const j of data2.cast)
      actor=actor.concat('<div class="card _peopleCustomCard"> <img class="card-img-top" src="https://image.tmdb.org/t/p/w600_and_h900_bestv2/',j.profile_path,'" alt="',j.name,'" style="width:100%" onclick="getDetailPerson(',j.id,')"> <div class="card-body" style="padding-left:10px;"> <h4 class="card-title">',j.name,'</h4> <p class="card-text">',j.character,'</p> </div> </div>');
      for (const j of data2.crew)
      if (j.department=="Directing")
      director=director.concat('<div class="card _peopleCustomCard"> <img class="card-img-top" src="https://image.tmdb.org/t/p/w600_and_h900_bestv2/',j.profile_path,'" alt="',j.name,'" style="width:100%"> <div class="card-body" style="padding-left:10px;"> <h4 class="card-title">',j.name,'</h4> <p class="card-text">',j.job,'</p> </div> </div>')
      $(".main_box").append(`
      <div class="_movieDetail">
      <div class="_movieInform" style="background-image: url('${"https://image.tmdb.org/t/p/original/"+data.backdrop_path}')">
        <div class="_movieInformImg">
        <img src="${"https://image.tmdb.org/t/p/w600_and_h900_bestv2/"+data.poster_path}" alt='${data.title}'>
        </div>
        <div class="_movieInformText">
          <strong>Title: </strong>${data.title} </br>
          <div style="display:block"><strong>Genres: </strong>${genre}</div>
          <strong>Release Date: </strong> ${data.release_date} </br>
        </div>
      </div>
      <div class="_movieOverview">
          <strong>Overview</strong></br>${data.overview}</br> </br>
          <strong>Actors: </strong> </br></br><div class="_castBox"> ${actor} </div></br>
          <strong>Director: </strong> </br></br> <div class="_castBox"> ${director} </div></br>
      </div>
    </div>
      `)
    })
    getReview(_movieid,0);
  })
}
function getDetailPerson(_personid){
  let url="".concat(baseURL,'person/',_personid,'?api_key=',APIKey,'&language=en-US');
  fetch(url)
  .then ((result)=>{
    return result.json();
  })
  .then ((data)=>{
    $(".main_box").empty();
    $("#_pagination").empty();
    $(".carousel").empty();
    $("#header2").empty();
    $("#header1").empty();
    document.getElementById("review").style.display="none";
    document.getElementById("_select-Box").style.display="none";
    let url2="".concat(baseURL,'person/',_personid,'/movie_credits?api_key=',APIKey,'&language=en-US');
    fetch(url2)
    .then ((result2)=>{
      return result2.json();
    })
    .then ((data2)=>{
      let movieCast="";
      for (const j of data2.cast)
      movieCast=movieCast.concat('<div class="card _peopleCustomCard" style="width:300px;"> <img class="card-img-top" src="https://image.tmdb.org/t/p/w600_and_h900_bestv2/',j.poster_path,'" alt="',j.title,'" style="width:100%" onclick="getDetail(',j.id,')"> <div class="card-body" style="padding-left:10px;"> <h4 class="card-title">',j.title,'</h4> <p class="card-text"><p><strong>Rated: </strong>',j.vote_average,'</p><p><strong>Release Date: </strong>',j.release_date,'</p></p> </div> </div>');
      $(".main_box").append(`
      <div class="_movieDetail">
      <div class="_movieInform" style="background-color:green">
        <div class="_movieInformImg">
        <img src="${"https://image.tmdb.org/t/p/w600_and_h900_bestv2/"+data.profile_path}" alt='${data.name}'>
        </div>
        <div class="_movieInformText">
          <strong>Name: </strong>${data.name} </br>
          <strong>Place of birth: </strong>${data.place_of_birth} </br>
        </div>
      </div>
      <div class="_movieOverview">
          <strong>Biography</strong></br>${data.biography}</br> </br>
          <strong>Participate in movies: </strong> </br></br><div class="_castBox"> ${movieCast} </div></br>
      </div>
    </div>
      `)
    })
  })
}
function getReview(_movieid, numb){
  numb = parseInt(numb); 
  document.getElementById("review").style.display="block";
  $("#reviewPlace").empty();
  $("#_pagination2").empty();
  let url="".concat(baseURL,'movie/',_movieid,'/reviews?api_key=',APIKey,'&language=en-US');
  fetch(url)
  .then ((result)=>{
    return result.json();
  })
  .then ((data)=>{
    if(data.total_results==0)
    {
      $("#reviewPlace").append(`<div style="color: white;padding:10px;width:max-content;font-size:20px"><strong>No review for this movie</strong></div>`);
    return;
    }
    for (let i = numb; i < parseInt(data.total_results) && i < numb + 2; i++)
    {
      $("#reviewPlace").append(`
      <div style="border-bottom:2px solid white;margin-top:10px;margin-bottom:10px">
      <div style="color: white;border:2px solid white; border-radius:10px;padding:10px;width:max-content"><strong>A review by ${data.results[i].author}</strong></div></br>
      <div class="review-content">${data.results[i].content}</div>
      <a href="${data.results[i].url}"><button class="_selectBox" style="width:15%;margin-bottom:0px">Read More</button></a>
      </div>
      `)
    }
    if (numb==0)
    $("#_pagination2").append(`<li class="page-item disabled"><a class="page-link" href="#reviewPlace">≪</a></li>`);
    else
    $("#_pagination2").append(`<li class="page-item"><a class="page-link" href="#reviewPlace" onclick="getReview(${_movieid},${numb-2})">≪</a></li>`);
    if (numb>=(parseInt(data.total_results)-2))
    $("#_pagination2").append(`<li class="page-item disabled"><a class="page-link" href="#reviewPlace">≫</a></li>`);
    else
    $("#_pagination2").append(`<li class="page-item"><a class="page-link" href="#reviewPlace" onclick="getReview(${_movieid},${numb+2})">≫</a></li>`);
  })
}